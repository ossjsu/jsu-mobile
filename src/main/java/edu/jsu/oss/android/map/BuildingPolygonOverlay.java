/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.map;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;

import com.google.android.maps.Overlay;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Projection;

public class BuildingPolygonOverlay extends Overlay {

	public boolean showBuildings;
	public boolean showParking;
	
	public BuildingPolygonOverlay(boolean buildings, boolean parking) {
		showBuildings = buildings;
		showParking = parking;
		// TODO remove parking stuff from this class
	}

	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow)
	{	
		Projection projection = mapView.getProjection();
	    Path p = new Path();
	    Point point = new Point();
	    
	    //define the paint
	    Paint polygonPaint = new Paint();
	    polygonPaint.setColor(0x55AE0000);
	    polygonPaint.setStyle(Style.FILL);
	    polygonPaint.setAntiAlias(true);
	    
	    //check to determine if the buildings layer is enabled
	    if (showBuildings){
		    ArrayList<ArrayList<GeoPoint>> buildings = BuildingPolygons.getBuildings();
		    
		    for (int i = 0; i < buildings.size(); i++){ //Building iterator (i)
			    for (int j = 0; j < buildings.get(i).size(); j++){ //GeoPoint iterator (j)
				    projection.toPixels(buildings.get(i).get(j), point);
				    if (j != 0)
					    p.lineTo(point.x, point.y);
				    else
				    	p.moveTo(point.x, point.y);
			    }
		    }
		    
		    //close the polygon
		    //p.close();
		    
		    //draw the polygon
		    canvas.drawPath(p, polygonPaint);
	    }
	}
	
	@Override
	public boolean onTap(GeoPoint point, MapView mapView){
		return false;
	}

}
