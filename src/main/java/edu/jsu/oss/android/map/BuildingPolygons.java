/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.map;

import java.util.ArrayList;
import com.google.android.maps.GeoPoint;

public final class BuildingPolygons {

	public static ArrayList<ArrayList<GeoPoint>> getBuildings(){
		ArrayList<ArrayList<GeoPoint>> buildings = new ArrayList<ArrayList<GeoPoint>>();	

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822659, -85765129));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822823, -85764870));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823383, -85765396));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823219, -85765671));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822659, -85765129));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823658, -85766449));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823490, -85766289));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823132, -85766930));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823311, -85767090));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823658, -85766449));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822689, -85767899));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822201, -85767487));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822472, -85767036));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822590, -85767159));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822418, -85767456));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822781, -85767761));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822689, -85767899));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822475, -85766571));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822208, -85767021));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821831, -85766670));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822109, -85766220));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822475, -85766571));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821949, -85766136));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821869, -85766258));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821381, -85765831));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821369, -85765411));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821686, -85764915));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821800, -85765030));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821522, -85765503));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821522, -85765739));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821949, -85766136));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821651, -85764854));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821735, -85764725));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821949, -85764931));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821869, -85765060));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821651, -85764854));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821846, -85764778));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821964, -85764603));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822350, -85764961));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822239, -85765152));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821846, -85764778));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822445, -85764404));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821793, -85764381));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821789, -85764053));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822449, -85764076));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822445, -85764404));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821091, -85766663));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821236, -85766563));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821651, -85767342));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821518, -85767456));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821091, -85766663));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823971, -85766380));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824047, -85765221));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824574, -85765274));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824558, -85765572));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824253, -85765533));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824203, -85766136));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824383, -85766159));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824368, -85766426));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823971, -85766380));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825405, -85766357));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825397, -85765739));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825268, -85765739));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825275, -85766357));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825405, -85766357));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824581, -85765701));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824394, -85765678));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824379, -85765991));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824570, -85766014));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824581, -85765701));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825405, -85767860));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825291, -85767860));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825279, -85767220));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825401, -85767220));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825405, -85767860));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825180, -85768532));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824490, -85768532));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824486, -85768311));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825180, -85768311));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825180, -85768532));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825169, -85768051));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824482, -85768059));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824482, -85767830));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825165, -85767838));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825169, -85768051));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825111, -85769257));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824554, -85769257));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824547, -85768761));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825104, -85768761));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825111, -85769257));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823944, -85766716));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823830, -85767342));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823669, -85767281));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823788, -85766655));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823944, -85766716));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823700, -85767685));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823601, -85767624));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823681, -85767410));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823788, -85767471));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823700, -85767685));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823441, -85768570));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823441, -85768288));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823860, -85768288));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823860, -85768570));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823441, -85768570));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823879, -85768921));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823689, -85768921));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823689, -85769058));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823509, -85769058));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823509, -85769447));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823692, -85769447));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823689, -85769676));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823887, -85769676));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823879, -85768921));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822388, -85772743));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822384, -85773651));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823376, -85773659));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823380, -85772758));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822388, -85772743));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822445, -85771973));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822445, -85771729));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822617, -85771729));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822620, -85771973));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822445, -85771973));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822800, -85771744));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822742, -85771675));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822865, -85771500));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822994, -85771660));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822865, -85771828));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822800, -85771744));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822849, -85771088));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822983, -85770912));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822838, -85770752));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822716, -85770935));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822849, -85771088));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822903, -85771454));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822899, -85771172));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823032, -85771164));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823032, -85771446));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822903, -85771454));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821457, -85771851));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821590, -85771698));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821472, -85771538));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821339, -85771698));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821457, -85771851));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822811, -85776619));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822960, -85776520));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823051, -85776352));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823078, -85776154));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823059, -85774963));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822948, -85774757));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822819, -85774689));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822639, -85774681));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822430, -85774750));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822319, -85774979));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822300, -85776154));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822353, -85776375));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822464, -85776558));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822651, -85776649));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822811, -85776619));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825520, -85775543));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825520, -85774582));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824429, -85774567));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824429, -85775528));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825520, -85775543));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825665, -85777260));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824245, -85777214));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824181, -85776810));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824203, -85776390));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824306, -85776001));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824493, -85775749));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824776, -85775627));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825230, -85775635));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825665, -85775658));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825665, -85777260));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824329, -85774147));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824329, -85773041));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823875, -85773041));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823879, -85774147));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824329, -85774147));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820038, -85769310));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820034, -85769096));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820381, -85769096));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820381, -85769310));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820038, -85769310));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819801, -85771027));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819801, -85770866));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819351, -85770874));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819351, -85771027));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819801, -85771027));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819740, -85768898));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819752, -85769691));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819599, -85769691));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819588, -85768906));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819740, -85768898));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819485, -85769585));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819080, -85769585));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819088, -85769737));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33818993, -85769737));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33818985, -85769440));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819481, -85769440));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819485, -85769585));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819901, -85765068));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819366, -85765106));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819489, -85767532));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820011, -85767502));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819901, -85765068));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819851, -85763840));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819881, -85763359));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820019, -85763359));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820030, -85763077));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820259, -85762863));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820400, -85763100));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820171, -85763321));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820271, -85763474));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820507, -85763298));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820641, -85763542));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820320, -85763817));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820309, -85763908));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820400, -85763924));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820389, -85764053));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820110, -85764000));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820122, -85763878));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819851, -85763840));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820892, -85764771));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820507, -85764633));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820621, -85764153));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820553, -85764130));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820641, -85763741));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821152, -85763924));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821098, -85764175));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821037, -85764160));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820892, -85764771));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33818981, -85762672));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33818642, -85762573));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33818676, -85762413));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819012, -85762512));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33818981, -85762672));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33818615, -85762848));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33818649, -85762688));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33818951, -85762772));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33818920, -85762917));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33818615, -85762848));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819050, -85762947));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819111, -85762581));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819233, -85762619));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819176, -85762978));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819050, -85762947));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819778, -85762337));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819859, -85762047));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819511, -85761902));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819443, -85762199));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819778, -85762337));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823360, -85762466));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823269, -85762413));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823414, -85761978));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823509, -85762032));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823360, -85762466));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824329, -85763557));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823971, -85763412));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824078, -85762924));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824451, -85763069));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824329, -85763557));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824921, -85763550));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824940, -85763329));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825138, -85763359));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825161, -85763077));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825230, -85763092));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825241, -85762939));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825161, -85762932));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825191, -85762611));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825699, -85762703));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825680, -85763008));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825340, -85762962));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825329, -85763100));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825439, -85763123));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825409, -85763412));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825577, -85763435));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825562, -85763641));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824921, -85763550));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826305, -85763130));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826180, -85762863));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826149, -85762604));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826199, -85762375));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826279, -85762215));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826962, -85762199));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826954, -85763115));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826756, -85763214));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826523, -85763229));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826305, -85763130));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826077, -85761833));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826065, -85761192));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826576, -85761192));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826588, -85761826));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826077, -85761833));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826469, -85760292));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826550, -85760292));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826550, -85760437));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826672, -85760437));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826672, -85760292));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826740, -85760292));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826740, -85759918));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826462, -85759926));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826469, -85760292));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33829960, -85761497));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33829960, -85761368));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33830059, -85761360));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33830044, -85760307));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33830475, -85760292));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33830479, -85760841));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33830360, -85760849));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33830360, -85760933));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33830261, -85760933));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33830269, -85761490));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33829960, -85761497));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828720, -85763367));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828571, -85763489));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828400, -85763382));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828201, -85763550));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828159, -85763474));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828110, -85763481));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828091, -85763176));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828140, -85763031));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828419, -85763199));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828491, -85763092));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828468, -85763039));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828411, -85763069));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828289, -85762680));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828499, -85762604));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828541, -85762688));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828571, -85762604));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828800, -85762657));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828720, -85763046));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828678, -85763031));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828659, -85763100));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828732, -85763191));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828972, -85762970));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33829029, -85763107));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33829041, -85763443));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828999, -85763420));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828960, -85763512));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33828720, -85763367));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33827530, -85763618));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33827049, -85763657));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33827030, -85763390));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33827511, -85763351));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33827530, -85763618));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826015, -85764473));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826111, -85764351));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826263, -85764534));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826172, -85764648));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826015, -85764473));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825550, -85764870));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825123, -85764862));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825119, -85764641));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825550, -85764641));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33825550, -85764870));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824360, -85764359));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824429, -85764076));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824638, -85764160));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824570, -85764427));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33824360, -85764359));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33830502, -85762657));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33829945, -85762672));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33829960, -85763298));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33830521, -85763275));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33830502, -85762657));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822784, -85762291));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822620, -85762199));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822681, -85762001));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822849, -85762077));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822784, -85762291));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820641, -85767380));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820122, -85767441));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820110, -85767258));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820278, -85767242));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820278, -85767189));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820438, -85767181));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820450, -85767227));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820633, -85767204));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820641, -85767380));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822102, -85765106));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821999, -85765030));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821911, -85765053));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821861, -85765152));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821880, -85765266));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821976, -85765343));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822071, -85765327));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822121, -85765228));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822102, -85765106));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826012, -85763893));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826015, -85763550));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826389, -85763557));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826385, -85763908));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33826012, -85763893));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820595, -85768608));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820469, -85768517));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820839, -85767792));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820950, -85767876));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33820595, -85768608));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819809, -85771400));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819809, -85771278));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819328, -85771278));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819336, -85771400));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819809, -85771400));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819309, -85771400));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819309, -85770981));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819214, -85770981));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819218, -85771400));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33819309, -85771400));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821667, -85777214));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821541, -85777206));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821529, -85776871));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821659, -85776871));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821667, -85777214));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821690, -85777023));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822060, -85777023));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822060, -85776825));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821690, -85776817));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33821690, -85777023));

		buildings.add(new ArrayList<GeoPoint>());
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823311, -85768417));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822941, -85768608));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33822830, -85768311));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823200, -85768120));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823219, -85768181));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823341, -85768028));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823299, -85767990));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823460, -85767677));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823608, -85767807));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823441, -85768112));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823383, -85768059));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823246, -85768242));
		buildings.get(buildings.size() - 1).add(new GeoPoint(33823311, -85768417));

		return buildings;
	}
	
}
