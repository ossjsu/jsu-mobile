/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.map;

import java.util.ArrayList;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;

import com.google.android.maps.Projection;
import com.google.android.maps.Overlay;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;

public class ParkingPolygonOverlay extends Overlay {

	public ParkingPolygonOverlay(){ }
	
	@Override
	public void draw(Canvas canvas, MapView mapView, boolean shadow)
	{	
		Projection projection = mapView.getProjection();
	    
	    //define the paint
	    Paint polygonPaint = new Paint();
	    polygonPaint.setStyle(Style.FILL);
	    polygonPaint.setAntiAlias(true);
	    
	    //set paint color and draw polygons
	    polygonPaint.setColor(0x55787878);
	    paintPolygons(ParkingPolygons.getSilverParking(), projection, polygonPaint, canvas);
	    
	    polygonPaint.setColor(0x55E6B800);
	    paintPolygons(ParkingPolygons.getGoldParking(), projection, polygonPaint, canvas);
	    
	    polygonPaint.setColor(0x55E66916);
	    paintPolygons(ParkingPolygons.getOrangeParking(), projection, polygonPaint, canvas);
	    
	    polygonPaint.setColor(0x55006600);
	    paintPolygons(ParkingPolygons.getGreenParking(), projection, polygonPaint, canvas);
	}
	
	private void paintPolygons(ArrayList<ArrayList<GeoPoint>> polygons, Projection projection, Paint polygonPaint, Canvas canvas){

	    Path p = new Path();
	    Point point = new Point();
	    
		for (int i = 0; i < polygons.size(); i++){ //Building iterator (i)
		    for (int j = 0; j < polygons.get(i).size(); j++){ //GeoPoint iterator (j)
			    projection.toPixels(polygons.get(i).get(j), point);
			    if (j != 0)
				    p.lineTo(point.x, point.y);
			    else
			    	p.moveTo(point.x, point.y);
		    }
	    }

	    canvas.drawPath(p, polygonPaint);
	}
	
}
