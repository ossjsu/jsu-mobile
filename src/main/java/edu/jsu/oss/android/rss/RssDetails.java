/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.rss;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebSettings.RenderPriority;
import android.webkit.WebView;
import edu.jsu.oss.android.R;

/**
 * Activity class used to display the details of an RSS story.  This receives a bundle which includes 
 * the feedType type and the permalink to the RSS news story.  It retrieves the article from the web, 
 * and then displays the story in a WebView with additional HTML formatting.
 */
public class RssDetails extends Activity {
	
	/** WebView displaying the article title, date, and story as HTML. */
	private WebView articleWebView;
	/** HTML representation of the WebView's contents. */
	private String htmlText;
	
	/**
	 * Sets the current settings for the webView rendering, and loads the webview data.  If it is fresh
	 * instance, the data is retrieved.  Otherwise, the data is loaded from extras.
	 * 
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
				
		super.onCreate(savedInstanceState);

		Bundle bundle = this.getIntent().getExtras();
		
		setContentView(R.layout.rssdetails);
		
		//turn all scripting off and speed up rendering
		articleWebView = (WebView)findViewById(R.id.rssArticleText);
		articleWebView.getSettings().setJavaScriptEnabled(false);
		articleWebView.getSettings().setPluginsEnabled(false);
		articleWebView.getSettings().setRenderPriority(RenderPriority.HIGH);
		
		final String savedHtml = (String)getLastNonConfigurationInstance();
		
		//Determine whether this is a fresh start or screen orientation change
		//and respond accordingly
		if(savedHtml != null)
			htmlText = savedHtml;
		else{
			htmlText = "";
			try{
				//get the HTML
				URL url;
				if(bundle.getInt("feedType") == 0)
					url = new URL("http://jsumobilefeed.appspot.com/newsparser?" + bundle.getString("articleLink"));
				else if(bundle.getInt("feedType") == 1)
					url = new URL("http://jsumobilefeed.appspot.com/chantyparser?" + bundle.getString("articleLink"));
				else
					url = new URL("BAD \"feedType\" IN BUNDLE");
				
				//read the HTML input
				InputStream in = url.openConnection().getInputStream();
				BufferedInputStream bis = new BufferedInputStream(in);
			    ByteArrayOutputStream buf = new ByteArrayOutputStream();
			    
			    int result = bis.read();
			    while(result != -1) {
			      byte b = (byte)result;
			      buf.write(b);
				  result = bis.read();
			    } 
			    //create the text for the WebView
			    htmlText = RssTag.openDocument() + RssTag.header(bundle.getString("articleTitle")) + RssTag.date(bundle.getString("articleDate")) + buf.toString() + RssTag.closeDocument();
			}
			catch (Exception ex){
				articleWebView.loadData("Exception occurred: " + ex.getMessage() + bundle.getString("articleLink"), "text/html", "utf-8");
			}
		}
		//load the HTML data into the webView
		articleWebView.loadData(htmlText, "text/html", "utf-8");
	}
	
	/**
	 * return the HTML of the article for a quick restoration OnCreate.
	 * 
	 * @see android.app.Activity#onRetainNonConfigurationInstance()
	 */
	@Override
	public Object onRetainNonConfigurationInstance() {
		//save the webview text for use on recreate
		return htmlText;
	}

}
