/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.rss;

import java.util.List;
import java.util.Vector;

/**
 * class representing the feed information for a given RSS source.  Also stores the RssItem contents
 * for each feed item listed.
 */
public class RssFeed {
	
	/** Title of the current RssItem.  */
	public String title;
	/** Description of the current RssItem.   */
	public String description;
	/**  Permalink to the current RssItem.  */
	public String link;
	/**  Publication Date of the current RssItem.  */
	public String pubdate;
	/**  List of all current RssItems given by the feed. */
	public List<RssItem> itemList;
	/**  Integer specifying which type of feed the current RssFeed is being used for. */
	public int feedType;
	
	/** static integer used to represent the News Feed. */
	public final int news = 0;
	/** static integer used to represent the Chanticleer */
	public final int chanty = 1;
	
	/**
	 * This constructor initializes all string fields to blank strings, so that they can be concatenated
	 * on subsequent calls.  This is necessary due to the face that the parser sometimes triggers multiple
	 * events within a single tag, depending on the size of the text and sometimes on special characters.
	 * 
	 * @param inType String specifying whether the feedType is used for the news
	 * feed or the Chanticleer.
	 */
	public RssFeed(int feedType){
		itemList = new Vector<RssItem>(0);
		this.feedType = feedType;
		
		title = "";
		link = "";
		pubdate = "";
	}
}
