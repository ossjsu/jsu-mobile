/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.rss;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

/**
 * AsyncTask implementation used specifically when an RSS item is being loaded.  This displays a wait dialog
 * to the user during the period of time while the parser is retrieving the RSS information.  After the 
 * information has been retrieved and parsed, the details activity is started.
 */
@SuppressWarnings("rawtypes")
public class RssItemLoadingTask extends AsyncTask {
	
	/** Context given by the calling activity. */
	private Context con;
	/** Contains the title, publication date, and hyperlink of an article. */
	private Bundle bun;
	/** Used to start the next activity - RssDetails. */
	private Intent RssDetailsIntent;
	/** Dialog displayed to the user to indicate that the article text is being retrieved. */
	private ProgressDialog waitDialog;
	
	/**
	 * @param context activity context
	 * @param bundle extras specifying information (such as feedType type, hyperlink, etc.) about the particular
	 * story being viewed.
	 */
	public RssItemLoadingTask(Context context, Bundle bundle){
		this.con = context;
		this.bun = bundle;
	}
	
	
	/**
	 * Displays the wait dialog to the user
	 * 
	 * @see android.os.AsyncTask#onPreExecute()
	 */
	@Override 
	protected void onPreExecute() {
		waitDialog = ProgressDialog.show(con, "Retrieving article", "Please wait...");
	}
	
	
	/**
	 * Starts the new activity and fires the OnCreate which gets and parses the data
	 * 
	 * @see android.os.AsyncTask#doInBackground(Params[])
	 */
	@Override
	protected Object doInBackground(Object... params) {
		
		RssDetailsIntent = new Intent(con, RssDetails.class);
		RssDetailsIntent.putExtras(bun);
		con.startActivity(RssDetailsIntent);
		
		return null;
	}
 
	/**
	 * Once the activity has been started (RSS read and parsed), dismiss the wait dialog.
	 * 
	 * @see android.os.AsyncTask#onPostExecute(java.lang.Object)
	 */
	@Override
	protected void onPostExecute(Object result) {
		waitDialog.dismiss();		
	}
}
