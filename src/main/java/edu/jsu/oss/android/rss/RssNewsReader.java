/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.rss;

import java.net.URL;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import android.app.ListActivity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import edu.jsu.oss.android.R;

/**
 * Displays a list of RSS story titles along with the date the stories were published on.  
 *
 */
public class RssNewsReader extends ListActivity {
	
	/** Holds attributes for both the feed and individual RssItems within the feed. */
	private RssFeed myRssFeed = null;
	
    /**
     * Determines which type of RSS feed has been started, and  parses the feed to gather story item
     * information if this is a fresh instance.  Otherwise, item information is loaded form extras.  The
     * feed object is then displayed using a new list adapter.
     * 
     * @see android.app.Activity#onCreate(android.os.Bundle)
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.rssmain);
        
        // load bundle with feedType and link to determine if it's news/chanty
        Bundle bundle = this.getIntent().getExtras();
                
        if(getLastNonConfigurationInstance() == null){
	        try {
	        	//Get the RSS feed and parse the data
	        	URL rssUrl = new URL(bundle.getString("link"));
				SAXParserFactory mySAXParserFactory = SAXParserFactory.newInstance();
				SAXParser mySAXParser = mySAXParserFactory.newSAXParser();
				
				XMLReader myXMLReader = mySAXParser.getXMLReader();
				RssHandler myRSSHandler = new RssHandler(bundle.getInt("feedType"));			
				myXMLReader.setContentHandler(myRSSHandler);
				
				InputSource myInputSource = new InputSource(rssUrl.openStream());
				myXMLReader.parse(myInputSource);
				
				myRssFeed = myRSSHandler.feed;
				
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
		else
			myRssFeed = (RssFeed) getLastNonConfigurationInstance();
		
		//Set the list adapter for this ListActivity
		if (myRssFeed!=null)
			setListAdapter(new RssListAdapter(this, R.layout.row, myRssFeed.itemList));	
    }
    
    /**
     * Save the RssFeed so no parsing is required upon orientation change or resume
     * 
     * @see android.app.Activity#onRetainNonConfigurationInstance()
     */
    @Override
    public Object onRetainNonConfigurationInstance(){
    	return myRssFeed;
    }

	/**
	 * Fire the details view activity that corresponds with the news item clicked.
	 * 
	 * @see android.app.ListActivity#onListItemClick(android.widget.ListView, android.view.View, int, long)
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {

		Bundle bundle = new Bundle();
		
		//put pertinent information in the bundle for the details view
		bundle.putString("articleTitle", myRssFeed.itemList.get(position).title);
		bundle.putString("articleLink", myRssFeed.itemList.get(position).link);
		bundle.putString("articleDate", myRssFeed.itemList.get(position).pubdate);
		bundle.putInt("feedType", myRssFeed.feedType);
		
		new RssItemLoadingTask(this, bundle).execute();
	}
    
	/**
	 * Custom list adapter using RssItems, is composed of a single TextView title and TextView
	 * publication date
	 *
	 */
	public class RssListAdapter extends ArrayAdapter<RssItem> {

		/**
		 * @param context activity context of the ListView
		 * @param textViewResourceId TextView to build listItems out of
		 * @param list list of story items
		 */
		public RssListAdapter(Context context, int textViewResourceId, List<RssItem> list) {
			super(context, textViewResourceId, list);	
		}

		/**
		 * Inflates the RSS view with the title and publication date to be loaded into the list, also
		 * changes the color for every other row.
		 * 
		 * @see android.widget.ArrayAdapter#getView(int, android.view.View, android.view.ViewGroup)
		 */
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			
			View row = convertView;
			 
			if(row==null){
				LayoutInflater inflater=getLayoutInflater();
				row=inflater.inflate(R.layout.row, parent, false);	
			}
			
			//set the text
			TextView listTitle=(TextView)row.findViewById(R.id.listtitle);
			listTitle.setText(myRssFeed.itemList.get(position).title);
			TextView listPubdate=(TextView)row.findViewById(R.id.listpubdate);
			listPubdate.setText(myRssFeed.itemList.get(position).pubdate);
			
			//Every other row is to be an off-gray color
			if (position%2 == 0){
				listTitle.setBackgroundColor(0xff101010);
				listPubdate.setBackgroundColor(0xff101010);
			}
			else{
				listTitle.setBackgroundColor(0xff080808);
				listPubdate.setBackgroundColor(0xff080808);
			}
			
			return row;
		}
	}
    
}