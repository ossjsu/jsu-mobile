/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.scoreboard;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public abstract class GamecockUpdater extends Thread {

	protected Handler scoreboardMessageHandler;
	protected int refreshTime;
	protected int sport;
	protected String homeName;
	protected String awayName;

	protected SAXParser parser;
	
	protected boolean continueUpdates;
	
	protected DefaultHandler parserHandler;
	
	public GamecockUpdater(Handler scoreboardMessageHandler, int refreshTime, int sport){
		this.scoreboardMessageHandler = scoreboardMessageHandler;
		this.refreshTime = refreshTime;
		
		this.sport = sport;		
		
		try {
			parser = SAXParserFactory.newInstance().newSAXParser();
		} catch (Exception e) {
			//Log.e(this.getName(), e.getLocalizedMessage());
		} 
		continueUpdates = true;
	}
	
	
	public void run(){
		while(continueUpdates){
			try{
				parser.parse(Sports.feeds[sport], parserHandler);
				
				sleep(refreshTime);
			} catch (Exception e) {
				//Log.e(this.getName(), e.getLocalizedMessage());
			} 
		}
	}
	
	public void killThread(){
		continueUpdates = false;
	}
	
	protected void sendMessageBundle(String text, int viewId){
		Bundle msg = new Bundle();
		msg.putString("text", text);
		msg.putInt("viewId", viewId);
		
		Message message = new Message();
		message.setData(msg);
		
		scoreboardMessageHandler.sendMessage(message);
	}
}