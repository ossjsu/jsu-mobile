/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.scoreboard;

import java.util.Calendar;
import java.util.GregorianCalendar;

// TODO use enums here - this is a horrible way to do this!
public final class Sports {

	public static int baseball = 0;
	public static int softball = 1;
	public static int football = 2;
	public static int volleyball = 3;
	
	public static int numSports = 4;
	
	//could store these as a sportInfo objects, but once again adding another layer would slow things
	//down and seems unnecessary since this doesn't require much scalability
	public static String[] feeds = {"http://www.jsugamecocksports2.com/livestats/baseball/jsu.xml", "http://www.jsugamecocksports2.com/livestats/softball/jsu.xml", "http://www.jsugamecocksports2.com/livestats/football/jsu.xml", "http://www.jsugamecocksports2.com/livestats/volleyball/jsu.xml"};
	//public static String[] feeds = {"http://jsumobilefeed.appspot.com/xml/baseball.xml", "http://jsumobilefeed.appspot.com/xml/softball.xml", "http://jsumobilefeed.appspot.com/xml/football.xml", "http://www.jsugamecocksports2.com/livestats/volleyball/jsu.xml"}; 
	public static String[] timeUnit = {"inning", "inning", "quarter", "game"};
	public static String[] sportname = {"Baseball", "Softball", "Football", "Volleyball"};
	public static String[] sportLink = {"http://jsugamecocksports.com/mobile/index.aspx?path=baseball", "http://jsugamecocksports.com/mobile/index.aspx?path=softball", "http://jsugamecocksports.com/mobile/index.aspx?path=football", "http://jsugamecocksports.com/mobile/index.aspx?path=wvball"};
	
	public static boolean inSeason(int sport){
		GregorianCalendar calendar = new GregorianCalendar();
		
		switch (sport){
		case 0: //Baseball
		case 1: //Softball
			if (calendar.get(Calendar.MONTH) > 1 && calendar.get(Calendar.MONTH) < 7)
				return true;
			break;
		case 2: //Football
		case 3: //Volleyball
			if (calendar.get(Calendar.MONTH) > 7)
				return true;
			break;
		}
		return false;
	}
}
