/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.scoreboard;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class VenueHandler extends DefaultHandler {

	public final int away_id = 0;
	public final int away_name = 1;
	public final int home_id = 2;
	public final int home_name = 3;
	public final int date = 4;
	public final int time = 5;
	
	public final int size = 6;
	
	public String[] venueInfo;
	private final String[] elementName = {"visid", "visname", "homeid", "homename", "date", "start"}; 
	
	public VenueHandler(){
		venueInfo = new String[size];
	}
	
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
	
		if(localName.equalsIgnoreCase("venue")){
			for (int i = 0; i < size; i++){
				venueInfo[i] = attributes.getValue(elementName[i]);
				//Log.i("VENUE_HANDLER",  "Got value " + venueInfo[i] + " from element " + elementName[i]);
			}
		}
	}
}
