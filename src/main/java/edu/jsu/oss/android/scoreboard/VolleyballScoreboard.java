/* 	
 *   JSU Mobile - Android application for the mobile delivery of public JSU information
 *   Copyright 2011 Josh Cain
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

package edu.jsu.oss.android.scoreboard;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;
import edu.jsu.oss.android.R;

public class VolleyballScoreboard extends Activity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.scoreboard_volleyball);
        
        digitizeFont((TextView)findViewById(R.id.volleyball_home_score));
        digitizeFont((TextView)findViewById(R.id.volleyball_away_score));
        digitizeFont((TextView)findViewById(R.id.volleyball_home_games_won));
        digitizeFont((TextView)findViewById(R.id.volleyball_away_games_won));
        digitizeFont((TextView)findViewById(R.id.volleyball_game_number));
	}

	private void digitizeFont(TextView view){
		if(view != null)
			view.setTypeface(Typeface.createFromAsset(getAssets(), "digital.ttf"));
	}
}
